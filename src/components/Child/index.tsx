import React, { Component } from 'react'
import Taro from '@tarojs/taro'
import { View } from '@tarojs/components'

type IState = {}

type IProps = {
    userName?: string
}

// export default class Child extends Component<IProps, IState> {
//   state: IState = {}

//   componentWillMount() {
    
//   }

//   componentDidShow() {
    
//   }

//   componentWillReceiveProps(nextProps) {
    
//   }

//   render() {
//       console.log(this.props)
//       const {userName} = this.props
//     return (
//       <View>
//           父级传递的数据为{userName}
//       </View>
//     )
//   }
// }
export default function Child(props:IProps) {
  return (
    <View>
        父级传递的数据为{props.userName}
    </View>
  )

}