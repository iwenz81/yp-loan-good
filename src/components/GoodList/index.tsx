import React, { Component } from 'react'
import Taro from '@tarojs/taro'
import {View, ScrollView, Image} from '@tarojs/components'
import GoodItem from '../GoodItem'
import './index.scss'

type IState = {}

type IProps = {}
let ImageLoadList = []
const img1 = 'https://feicui-common.oss-cn-shenzhen.aliyuncs.com/homeActivity/20210309180556153320497.jpg'
const img2 = 'https://feicui-common.oss-cn-shenzhen.aliyuncs.com/homeProjectile/20210325160949491387798.jpg'

const goodsList =
  [
    {
      image: img1 ,
      id:0,
      value:'1'
    },
    {
      image: img1,
      id:1,
      value:'2'
    },
    {
      image: img2,
      id:2,
      value:'3'
    },
    {
      image: img1,
      id:3,
      value:'4'
    },
    {
      image: img1 ,
      id:4,
      value:'5'
    },
    {
      image: img1,
      id:5,
      value:'6'
    },
    {
      image: img1 ,
      id:6,
      value:'7'
    },
    {
      image: img2,
      id:7,
      value:'8'
    },
    {
      image: img2,
      id:8,
      value:'9'
    },
    {
      image: img1,
      id:9,
      value:'10'
    },
    {
      image: img1,
      id:10,
      value:'11'
    },
    {
      image: img2,
      id:11,
      value:'12'
    },
    {
      image: img2,
      id:12,
      value:'13'
    },
    {
      image: img1,
      id:13,
      value:'14'
    },
    {
      image: img1,
      id:14,
      value:'15'
    },

  ]

export default class GoodList extends Component<IProps, IState> {
  state:any = {
    data: [],
    lenght: goodsList.length,
    goodsLeft: [],
    goodsRight: [],
    ImageLoadList: [],
    imgWidth: 0
  }
  componentWillMount() {
    Taro.getSystemInfo({
      success: (res => {
          console.log('查看图标的宽度',res)
        let ww = res.windowWidth
        let wh = res.windowHeight
        let imgWidth = ww * 0.4
        this.setState({
          imgWidth
        })
      })
    })
  }

  componentDidShow() {
    
  }

  componentWillReceiveProps(nextProps) {
    
  }

  onImageLoad = (e) => {
    console.log(e.currentTarget.id)
    let oImgW = e.detail.width;         //图片原始宽度
    let oImgH = e.detail.height;        //图片原始高度
    let imgWidth = 343;  //图片设置的宽度
    let scale = imgWidth / oImgW;        //比例计算
    let imgHeight = oImgH * scale;      //自适应高度

    //初始化ImageLoadList数据
    ImageLoadList.push({
      id: parseInt(e.currentTarget.id),
      height: imgHeight,
    })
    //载入全部的图片进入ImageLoadList数组，若数量和goodsList中相等，进入图片排序函数
    if (ImageLoadList.length === goodsList.length) {
      this.handleImageLoad(ImageLoadList)
    }
    // console.log(ImageLoadList)
  }
  handleImageLoad = (ImageLoadList:any) => {
    console.log('hello', ImageLoadList)
    //对无序的列表进行排序
    for (let i = 0; i < ImageLoadList.length - 1; i++)
      for (let j = 0; j < ImageLoadList.length - i - 1; j++) {
        if (ImageLoadList[j].id > ImageLoadList[j + 1].id) {
          let temp = ImageLoadList[j]
          ImageLoadList[j] = ImageLoadList[j + 1]
          ImageLoadList[j + 1] = temp
        }
      }
    //现在的列表在goodList的基础上，多了height属性
    console.log('ImageLoadList', ImageLoadList);
    //为现在的列表添加value值

    for (let i = 0; i < goodsList.length; i++) {
      ImageLoadList[i].value = goodsList[i].value
      ImageLoadList[i].image = goodsList[i].image
      console.log('ImageLoadList[i].height', ImageLoadList[i].height)
      ImageLoadList[i].imgStyle = {height: ImageLoadList[i].height + 'rpx'}

    }
    console.log('ImageLoadList', ImageLoadList);
    //对现在的列表进行操作
    let leftHeight = 0;
    let rightHeight = 0;
    let left = []
    let right = []
    //遍历数组
    for (let i = 0; i < ImageLoadList.length; i++) {
      console.log('左边的高度', leftHeight, '右边边的高度', rightHeight)
      if (leftHeight <= rightHeight) {
        console.log('第', i + 1, '张放左边了')
        left.push(ImageLoadList[i])
        leftHeight += ImageLoadList[i].height
        console.log('left', left);
      } else {
        console.log('第', i + 1, '张放右边了')
        right.push(ImageLoadList[i])
        rightHeight += ImageLoadList[i].height
        console.log('right', right);
      }
    }
    this.setState({
      goodsRight: right,
      goodsLeft: left
    }, () => {
      console.log(this.state);
    })
  }

  render() {
    const {goodsRight, goodsLeft} = this.state
    return (
        <View className="goods">
        <View style={{display: 'none'}}>
          {
            goodsList.map((item, index) => {
              return (
                <Image onLoad={this.onImageLoad} key={index} id={index} src={item.image}></Image>
              )
            })
          }
        </View>

        <ScrollView className="left">
          {
            <View className={'goods-left'}>
              {
                goodsLeft.map((item, index) => {
                  return (
                    <View key={index}>
                      <GoodItem item={item}></GoodItem>
                    </View>
                  )
                })
              }
            </View>
          }
        </ScrollView>

        <ScrollView className="right">
          {
            <View className={'goods-right'}>
              {
                goodsRight.map((item, index) => {
                  return (
                    <View key={index}>
                      <GoodItem item={item}></GoodItem>
                    </View>
                  )
                })
              }
            </View>
          }
        </ScrollView>

      </View>
    )
  }
}