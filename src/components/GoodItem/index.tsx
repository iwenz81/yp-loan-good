import React, { Component } from 'react'
import Taro from '@tarojs/taro'
import {View, Image} from '@tarojs/components'

type IState = {}

type IProps = {
  item: any
}

export default class  extends Component<IProps, IState> {
  state: IState = {}

  componentWillMount() {
    
  }

  componentDidShow() {
    
  }

  componentWillReceiveProps(nextProps) {
    
  }

  render() {
    const {item} = this.props
    return (
        <View className={'goods-item'}>
            <Image src={item.image} className="goods-img" style={item.imgStyle} mode="widthFix" />
            <View className="goods-title two-text">故宫上新感温变色手链女纯银手饰ins小众设…</View>
            <View className="price">¥{item.value}</View>
        </View>
    )
  }
}