import React, { Component } from 'react'
import Taro from '@tarojs/taro'
import { View } from '@tarojs/components'
import { AtTabs } from 'taro-ui'
import {TabType} from '../../types/indexType'
import './index.scss'

type IState = {
    current: number
}

type IProps = {
    tabs: TabType[]
    tabsClick: Function
}

export default class TabsMenu extends Component<IProps, IState> {
  state: IState = {
    current: 0
  }
  handleClick = (current) => {
    //   console.log(current)
    this.setState({
        current
    })
    this.props.tabsClick(current)
    console.log(this.props)
  }

  componentWillMount() {
    
  }

  componentDidShow() {
    
  }

  componentWillReceiveProps(nextProps) {
    
  }

  render() {
      const {tabs} = this.props
    return (
      <View className='tabs-warp'>
        <AtTabs
        current={this.state.current}
        scroll
        tabList={tabs}
        onClick={this.handleClick}>
        </AtTabs>
      </View>
    )
  }
}