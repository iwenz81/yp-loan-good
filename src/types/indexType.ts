export interface TabType {
    id: string
    name?: string
    title: string
}