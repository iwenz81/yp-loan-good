export default {
  pages: [
    'pages/index/index',
    'pages/orders/index',
    'pages/my/index',
    'pages/blog/index',
  ],
  tabBar: {
    list: [
      {
        pagePath: "pages/index/index",
        iconPath: "static/images/tabBar/index.png",
        selectedIconPath: "static/images/tabBar/index-active.png",
        text: "首页"
      },
      {
        pagePath: "pages/orders/index",
        iconPath: "static/images/tabBar/orders.png",
        selectedIconPath: "static/images/tabBar/orders-active.png",
        text: "管理"
      },
      {
        pagePath: "pages/my/index",
        iconPath: "static/images/tabBar/my.png",
        selectedIconPath: "static/images/tabBar/my-active.png",
        text: "个人中心"
      },
    ],
    color: "#666464",
    selectedColor: "#1296db",
    borderStyle: "black",
    backgroundColor: "#ffffff"
  },
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: 'WeChat',
    navigationBarTextStyle: 'black',
  },
  subPackages:[
    // {
    //   root: "pages/pageA/",
    //   pages: [
    //     "index",
    //     "pageA1",
    //   ]
    // }
  ]
}
