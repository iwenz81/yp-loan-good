import { Component } from 'react'
import Taro from '@tarojs/taro'
import { View, Text } from '@tarojs/components'
import { AtButton } from 'taro-ui'
import Child from '../../components/Child'
import TabsMenu from '../../components/TabsMenu'
import GoodList from '../../components/GoodList'

import "taro-ui/dist/style/components/button.scss" // 按需引入
import './index.scss'

export default class Index extends Component {

  state = {
    blogId: 1222,
    tabs:[
        { id: '1', title: '珠宝' },
        { id: '2', title: '和田玉' },
        { id: '3', title: '吊坠' },
        { id: '4', title: '国画' },
        { id: '5', title: '玉石珠宝' },
        { id: '6', title: '佛像观音' }
    ]
  }
  componentWillMount () { }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }
  toBlog = ()=>{
    Taro.navigateTo({ url: `/pages/blog/index?id=${this.state.blogId}` })
  }
  tabsClick = (current) => {
    console.log(current)
  }

  render () {
    const {tabs} = this.state
    return (
      <View className='pages'>
        <View className="tabs-warpper">
          <TabsMenu tabs={tabs} tabsClick={this.tabsClick}></TabsMenu>
        </View>
        <View className="content">
          <GoodList></GoodList>
        </View>
        
        {/* <Text>Hello world!</Text>
        <AtButton type='primary' onClick={this.toBlog}>I need Taro UI</AtButton>
        <Child userName={'Excellent'}></Child> */}
      </View>
    )
  }
}
