import React, { Component,useState,useEffect } from 'react'
import Taro,{useRouter} from '@tarojs/taro'
import { View,Text,Image } from '@tarojs/components'
import Child from '../../components/Child'
import {xiedajiao,liuying} from '../../tools'
// import screenIcon from '../../static/images/icon/screen_icon.png'

function Blog(props){
    const [blogId,setBlogId] = useState('')
    const  [articleList,setArticleList] = useState([])
    const router = useRouter()
    useEffect(()=>{
        console.log('1父组件',props)
        xiedajiao()
        setBlogId(router.params.id)
      },[])
      const testHandler= ()=>{
        Taro.request({
            url:'https://apiblog.jspang.com/default/getArticleList'
        }).then(res=>{
            console.log(res.data)
            setArticleList(res.data.list)
        })
    }
    return (
        <View>
            <Text onClick={testHandler}>Blog Page{blogId}</Text>
            <Child userName={'Excellent'}></Child>
            <ul>
                {
                    articleList.map(item=>{
                        return <li key={item.id}>{item.title}</li>
                    })
                }
            </ul>
            
            {/* <Image src={require('../../static/images/icon/screen_icon.png')} style='width: 300px;height: 100px;background: #fff;' /> */}
        </View>
    )
}
export default Blog