import { Component } from 'react'
import { View, Text } from '@tarojs/components'
import { AtButton } from 'taro-ui'
import TabsMenu from '../../components/TabsMenu'
import {replaceName} from '../../utils/index'
import OrderList from './components/OrderList'

import './index.scss'

export default class Index extends Component {
  state = {
    tabs:[
      {id: '1',title: '全部'},
      {id: '2',title: '待审核'},
      {id: '3',title: '待取货'},
      {id: '4',title: '配送中'},
      {id: '5',title: '待完成'},
    ]
  }
  componentWillMount () { }

  componentDidMount () { 
  }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }
  tabsClick = (e) => {
    console.log(e)
  }

  render () {
    const {tabs} = this.state 
    return (
      <View className='index'>
        <View className="tabs-warpper tabs-warpper-width">
          <TabsMenu tabs={tabs} tabsClick={this.tabsClick}></TabsMenu>
        </View>
        <View className="orders content">
          <OrderList></OrderList>
        </View>
      </View>
    )
  }
}
