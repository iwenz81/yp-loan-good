import React, { Component } from 'react'
import Taro from '@tarojs/taro'
import { View,Image,Text } from '@tarojs/components'
import './index.module.scss'

type IState = {}

type IProps = {}

export default class OrderList extends Component<IProps, IState> {
  state: IState = {}

  componentWillMount() {
    
  }

  componentDidShow() {
    
  }

  componentWillReceiveProps(nextProps) {
    
  }

  render() {
      const list = [1,2,3,4,5,6,7]
      const img1 = 'https://feicui-common.oss-cn-shenzhen.aliyuncs.com/homeActivity/20210309180556153320497.jpg'
    return (
      <View className='order list'>
          {
              list.map(item=>{
                  return (
                      <View key={item} className="item">
                          <Image src={img1} className="goods-img" mode="aspectFill" />
                          <View className="right">
                            <View className="title two-text">故宫上新感温变色手链女纯银手饰ins小众设计原矿绿松石天蓝…</View>
                            <View className="flex time">
                                <View className="time-name">04.12 14:00-04.13 14:00</View>
                                <View className="statu">待审核</View>
                            </View>
                          </View>
                      </View>
                  )
              })
          }
      </View>
    )
  }
}